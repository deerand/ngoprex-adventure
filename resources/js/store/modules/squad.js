import axios from 'axios';
const state = {
  squads: [],
  statusImage: null
}

const getters = {
  getAllSquad: (state) => state.squads
}

const actions = {
  fetchSquads({ commit }) {
    const response = axios.get('api/squads')
      .then(res =>  commit('fetchSquads', res.data));
  },
  addSquad({ commit }, formData) {
    const response = axios.post('api/squads', formData);
    commit('addSquad', response.data)
  },
  deleteSquad({ commit }, id_squad) {  
    axios.delete(`api/squads/${id_squad}`);
    commit('deleteSquad', id_squad);
  },
  updateSquad({ commit }, formData)
  {
    
    axios.post(`api/squads/5`, formData);
  }
}

const mutations = {
  fetchSquads: (state, squads) => state.squads = squads,
  addSquad: (state, squad) => state.squads.unshift(squad),
  deleteSquad: (state, id_squad) => (state.squads = state.squads.filter(squad => squad.id_squad !== id_squad)),  
}

export default {
  state,
  getters,
  actions,
  mutations
}