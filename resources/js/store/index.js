import Vue from 'vue';
import Vuex from 'vuex';
import squad from './modules/squad';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    squad
  }
})