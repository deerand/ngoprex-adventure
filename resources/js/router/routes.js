import Vue from 'vue';
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import FrontLayout from '../components/depan/FrontLayout'
import Home from '../components/content/Home';
import formSquad from '../components/content/formSquad';
import listSquads from '../components/content/listSquads'
import editSquad from '../components/content/editSquad';

const routes = [{
  path: '/',  
  component: FrontLayout,
    children: [
      {
        path: '/',
        component: Home,
        name: 'home'
      },
      {
        path: '/formSquad',
        component: formSquad,
        name: 'formSquad'
      },
      {
        path: '/listSquads',
        component: listSquads,
        name: 'listSquads'
      },
      {
        path: ':id',
        component: editSquad,
        name: 'editSquad'
      },
  ]
},
];
export const router = new VueRouter({  
  routes,
  mode: 'history'
});
