<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ngoprex Adventure</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
  body {
    background-color: white;
  }
  </style>
</head>
<body class="app header-fixed sidebar-md-show sidebar-fixed">
  <div id="app">
    <router-view/>
  </div>
  <script src="{{asset('js/app.js')}}"></script>  
</body>
</html>