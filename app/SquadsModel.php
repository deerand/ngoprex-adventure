<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SquadsModel extends Model
{
    protected $table = 'squads';
    protected $primaryKey = 'id_squad';
    protected $fillable = [
        'fullname','nickname','email','contact','photo'
    ];
}
