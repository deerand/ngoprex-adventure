<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use App\Http\Requests\UserRequest;
use App\User;

class AuthController extends Controller
{  
	public function register(UserRequest $request)
  {    
    $name = $request->name;
    $email = $request->email;
    $password = $request->password;
    $user = User::create([
      'name' => $name,
      'email' => $email,
      'password' => Hash::make($password),
    ]);
    return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
  }

  public function login(Request $request)
  {

    $credentials = $request->only('email', 'password');
    $credentials['is_verified'] = 1;

    $rules = [
      'email' => 'required|email',
      'password' => 'required',
    ];

    $messages = [
      'required' => ':attribute Harus di Isi',
      'email' => 'Format Email Tidak Valid'
    ];

    $validator = Validator::make($credentials, $rules, $messages);
    if ($validator->fails()) {
      return response()->json([
        'errorMessages' => $validator,
        'status' => false
      ]);
    } else {
      try {
        if (! $token = JWTAuth::attempt($credentials)) {
          return response()->json(['success' => false, 'error' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.'], 401);
        }
      } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to login, please try again.'], 500);
      }
    }
      return response()->json(['success' => true, 'data'=> [ 'token' => $token ]]);    
    }
}
