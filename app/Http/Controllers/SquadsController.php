<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SquadsModel;
use Image;
use Illuminate\Support\Facades\Input;
use Validator;
use File;

class SquadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $squad = SquadsModel::all();
        return response()->json($squad,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'filename' => 'required' 
        ]);
        $originalImage= $request->file('filename');
        $thumbnailImage = Image::make($originalImage);
        $thumbnailPath = public_path().'/thumbnail/';
        $originalPath = public_path().'/images/';
        $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
        $thumbnailImage->resize(150,150);
        $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName()); 

        $squad= new SquadsModel();
        $squad->fullname = $request->input('fullname');
        $squad->nickname = $request->input('nickname');
        $squad->email = $request->input('email');
        $squad->contact = $request->input('contact');
        $squad->filename=time().$originalImage->getClientOriginalName();
        $squad->save();

        return back()->with('success', 'Your images has been successfully Upload');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $squad = SquadsModel::findOrFail($id)->first();
        return response()->json($squad,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $squad = SquadsModel::findOrfail($id);
        $squad->fullname = $request->input('fullname');
        $squad->nickname = $request->input('nickname');
        $squad->email = $request->input('email');
        $squad->contact = $request->input('contact');
        $squad->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    try{
        $delSquad = SquadsModel::find($id);
        $deleteFilename = array();
        $deleteFilename[0] = public_path().'/images/'.$delSquad->filename;
        $deleteFilename[1] = public_path().'/thumbnail/'.$delSquad->filename;
        foreach($deleteFilename as $key => $value)
        {            
        File::delete($value);
        }
        $delSquad->delete();

    } catch (\Exception $e){
        if($e->getCode() == "23000"){
            return response()->json(['error' => true, 'message' => 'Failed, Other Data Refrence this data']);
        }
        return response()->json(['error' => true, 'message' => $e->getMessage()]);
    }

    return response()->json(['error' => false, 'message' => 'product success deleted']);

    }
}
